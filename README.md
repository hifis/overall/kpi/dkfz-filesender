# Service Usage

* DKFZ *FileSender*

## Plotting

* Plotting is be performed in the [Plotting project](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

./stats/usage-stats-filesender-weekly.csv - schedule: weekly

| Data | Unit | Comment | Weighting |
| ----- | ----- | ----- | ----- |
| user count | Quantity/Number | Nr of users in total | 25% |
| active external users | Quantity/Number | Nr of active external users in the last 7 days | 25% |
| available transfers | Quantity/Number | Nr of currently available transfers | 6.25% |
| created transfers | Quantity/Number | Nr of created transfers in total | 6.25% |
| created transfers by active external users | Quantity/Number | Nr of created transfers by external users in the last 7 days | 6.25% |
| files uploaded | Quantity/Number | Nr of uploaded files in total | 6.25% |
| files downloaded | Quantity/Number | Nr of downloaded files in total | 6.25% |
| transfered up | Quantity/Number | Uploaded bytes in total | 6.25% |
| transfered down | Quantity/Number | Downloaded bytes in total | 6.25% |
| used space | Quantity/Number | Currently used space in bytes | 6.25% |

## Schedule

* weekly
