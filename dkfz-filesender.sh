#!/bin/bash

DB_NAME=$FILESENDER_DB_NAME
DB_USER=$FILESENDER_DB_USER
DATETIME=$(date --rfc-3339=seconds)

USER_COUNT=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT COUNT(id) FROM authentications WHERE saml_user_identification_uid ~ '.*@(?!localhost.localdomain).*'")
ACTIVE_EXTERNAL_USERS=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT COUNT(DISTINCT(author_id)) FROM auditlogs JOIN authentications ON CAST(auditlogs.author_id AS integer) = authentications.id WHERE author_type = 'User' AND saml_user_identification_uid LIKE '%@login.helmholtz.de' AND auditlogs.created >= CURRENT_DATE AT time zone 'UTC' - interval '7 days'")
AVAILABLE_TRANSFERS=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT COUNT(id) FROM transfers WHERE status='available'")
CREATED_TRANSFERS=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT count(id) FROM statlogs WHERE event='transfer_available'")
CREATED_TRANSFERS_ACTIVE_EXTERNAL_USERS=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT COUNT(auditlogs.id) FROM auditlogs JOIN authentications ON CAST(auditlogs.author_id AS integer) = authentications.id WHERE author_type = 'User' AND auditlogs.event = 'transfer_available' AND saml_user_identification_uid LIKE '%@login.helmholtz.de' AND auditlogs.created >= CURRENT_DATE AT time zone 'UTC' - interval '7 days';")
FILES_UPLOADED=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT count(id) FROM statlogs WHERE event='file_uploaded'")
FILES_DOWNLOADED=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT count(id) FROM statlogs WHERE event='download_ended'")
TRANSFERED_UP=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT sum(size) FROM statlogs WHERE event='upload_ended'")
TRANSFERED_DOWN=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT sum(size) FROM statlogs WHERE event='download_ended'")
USED_SPACE=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT size FROM statlogs WHERE event='global_storage_usage' ORDER BY id DESC limit 1")

echo $DATETIME,$USER_COUNT,$ACTIVE_EXTERNAL_USERS,$AVAILABLE_TRANSFERS,$CREATED_TRANSFERS,$CREATED_TRANSFERS_ACTIVE_EXTERNAL_USERS,$FILES_UPLOADED,$FILES_DOWNLOADED,$TRANSFERED_UP,$TRANSFERED_DOWN,$USED_SPACE, >> ./stats/usage-stats-dkfz-filesender-weekly.csv
